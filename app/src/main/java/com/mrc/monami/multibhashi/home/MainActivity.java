package com.mrc.monami.multibhashi.home;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import org.simmetrics.StringMetric;
//import org.simmetrics.metrics.StringMetrics;

import com.mrc.monami.multibhashi.BaseApp;
import com.mrc.monami.multibhashi.R;
import com.mrc.monami.multibhashi.model.ApiData;
import com.mrc.monami.multibhashi.model.LessonDatum;
import com.mrc.monami.multibhashi.networking.NetworkService;

import org.apache.commons.text.similarity.LevenshteinDistance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseApp implements LessonView {
    @Inject
    public NetworkService networkService;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.frame_layout)
    FrameLayout frameLayout;
    @BindView(R.id.play_audio_btn)
    ImageButton playAudioBtn;
    @BindView(R.id.concept_textView)
    TextView conceptTextView;
    @BindView(R.id.target_script_textView)
    TextView targetScriptTextView;
    @BindView(R.id.record_audio_btn)
    ImageButton recordAudioBtn;
    @BindView(R.id.tap_to_speak_text)
    TextView tapToSpeakText;
    @BindView(R.id.next_btn)
    FloatingActionButton nextButton;
    private int i;
    MediaPlayer mediaPlayer;
    String pronunciation = "";

    private static final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependency().inject(this);
        i = 0;
        renderView();
        LessonPresenter lessonPresenter = new LessonPresenter(networkService, this);
        lessonPresenter.getLessonData();

    }

    //Get voice input from user
    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Let's practise!");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainActivity.this, "Activity not found", Toast.LENGTH_SHORT).show();

        }
    }

    //Match voice input using Levenshtein Distance algorithm
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
                    int r = levenshteinDistance.apply(pronunciation, result.get(0).toLowerCase());
                    float percentage = (1 - ((float) r / Math.max(result.get(0).length(), pronunciation.length()))) * 100;
                    //Display percentage of audio match
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    if (percentage > 0.00f && percentage <= 50.00f) {
                        alertDialog.setTitle("Nice Try!");
                    } else if (percentage > 50.00f && percentage <= 75.00f) {
                        alertDialog.setTitle("Pretty Good!");
                    } else if (percentage > 75.00f && percentage <= 90.00f) {
                        alertDialog.setTitle("Great Job!");
                    } else if (percentage > 90.00f && percentage <= 99.00f) {
                        alertDialog.setTitle("Almost Perfect!");
                    } else if (percentage == 100.00f) {
                        alertDialog.setTitle("Perfect!");
                    } else {
                        alertDialog.setTitle("Practice Makes Perfect!");
                    }
                    alertDialog.setMessage("Audio Matched: " + String.valueOf((int) Math.ceil(percentage)) + " %");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();


                }
                break;
        }
    }

    //Render view on create
    public void renderView() {
        setContentView(R.layout.lesson_item_layout);
        ButterKnife.bind(this);

    }

    //Display progress bar
    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    //Hide progress bar
    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Snackbar.make(frameLayout, appErrorMessage, Snackbar.LENGTH_INDEFINITE)
                .show();
    }

    @Override
    public void getLessonDataSuccess(ApiData lessonDatum) {
        final List<LessonDatum> tempList = lessonDatum.getLessonData();
        itemSetter(tempList, i);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i < (tempList.size() - 1)) {
                    i++;
                    if (i == tempList.size() - 1) {
                        nextButton.setEnabled(false);
                        Snackbar.make(frameLayout, "No more data available", Snackbar.LENGTH_SHORT)
                                .show();
                    }
                    itemSetter(tempList, i);
                }
            }
        });


    }

    //Set items
    private void itemSetter(List<LessonDatum> lessonDatum, int position) {

        conceptTextView.setText(lessonDatum.get(position).getConceptName());
        targetScriptTextView.setText(lessonDatum.get(position).getTargetScript());
        pronunciation = lessonDatum.get(position).getPronunciation();

        final String audioUrl = lessonDatum.get(position).getAudioUrl();
        playAudioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudioBtn.setEnabled(false);
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(audioUrl);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    Toast.makeText(MainActivity.this,
                            "Playing",
                            Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        playAudioBtn.setEnabled(true);
                    }
                });

            }
        });

        String lessonType = lessonDatum.get(position).getType();
        if (lessonType.equals("learn")) {
            recordAudioBtn.setVisibility(View.GONE);
            tapToSpeakText.setVisibility(View.GONE);
        } else {
            recordAudioBtn.setVisibility(View.VISIBLE);
            tapToSpeakText.setVisibility(View.VISIBLE);
            recordAudioBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startVoiceInput();
                }
            });
        }

    }
}
