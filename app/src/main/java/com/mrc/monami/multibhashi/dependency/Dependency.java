package com.mrc.monami.multibhashi.dependency;

import com.mrc.monami.multibhashi.home.MainActivity;
import com.mrc.monami.multibhashi.networking.NetworkClient;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkClient.class})
public interface Dependency {
    void inject(MainActivity mainActivity);
}
