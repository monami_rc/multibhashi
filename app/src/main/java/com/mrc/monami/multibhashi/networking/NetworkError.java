package com.mrc.monami.multibhashi.networking;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class NetworkError extends Throwable{
    public static final String DEFAULT_ERROR_MESSAGE = "Something went wrong. Please try again.";
    public static final String NETWORK_ERROR_MESSAGE = "No Internet Connection";
    private final Throwable error;

    public NetworkError(Throwable e) {
        super(e);
        this.error = e;
    }

    public String getMessage() {
        return error.getMessage();
    }

    public boolean isAuthFailure() {
        return error instanceof HttpException &&
                ((HttpException) error).code() == HTTP_UNAUTHORIZED;
    }

    public boolean isResponseNull() {
        return error instanceof HttpException && ((HttpException) error).response() == null;
    }

    public String getAppErrorMessage() {

        if (this.error instanceof IOException){
            return NETWORK_ERROR_MESSAGE;
        }
        if (!(this.error instanceof HttpException)) {
            return DEFAULT_ERROR_MESSAGE;
        }

        return DEFAULT_ERROR_MESSAGE;
    }


    public Throwable getError() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NetworkError n = (NetworkError) o;
        return error != null ? error.equals(n.error) : n.error == null;

    }

    @Override
    public int hashCode() {
        return error != null ? error.hashCode() : 0;
    }
}
