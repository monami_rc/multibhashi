package com.mrc.monami.multibhashi.networking;

import com.mrc.monami.multibhashi.model.ApiData;
import com.mrc.monami.multibhashi.model.LessonDatum;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class NetworkService {
    private final NetworkInterface networkInterface;

    public NetworkService(NetworkInterface networkInterface) {
        this.networkInterface = networkInterface;
    }
    //Subscribe to lesson data, retry on failure
    public Subscription getLessonData(final GetLessonDataCallback lessonDataCallback){

        return networkInterface.getLessonData()
                .retryWhen(new RetryWithDelay(3, 2000))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ApiData>>() {
                    @Override
                    public Observable<? extends ApiData> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ApiData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        lessonDataCallback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(ApiData lessonDatum) {
                        lessonDataCallback.onSuccess(lessonDatum);

                    }
                });
    }

    public interface GetLessonDataCallback{
        void onSuccess(ApiData lessonDatum);
        void onError(NetworkError networkError);
    }
}
