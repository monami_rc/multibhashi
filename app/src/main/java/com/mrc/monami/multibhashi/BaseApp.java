package com.mrc.monami.multibhashi;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mrc.monami.multibhashi.dependency.DaggerDependency;
import com.mrc.monami.multibhashi.dependency.Dependency;
import com.mrc.monami.multibhashi.networking.NetworkClient;

import java.io.File;

public class BaseApp extends AppCompatActivity {
    Dependency dependency;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        dependency = DaggerDependency.builder().networkClient(new NetworkClient(cacheFile)).build();
    }

    public Dependency getDependency() {
        return dependency;
    }
}
