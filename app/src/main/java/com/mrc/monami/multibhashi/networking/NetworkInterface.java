package com.mrc.monami.multibhashi.networking;

import com.mrc.monami.multibhashi.model.ApiData;
import com.mrc.monami.multibhashi.model.LessonDatum;

import retrofit2.http.GET;
import rx.Observable;

public interface NetworkInterface {
    @GET("/getData.php")
    Observable<ApiData> getLessonData();

}
