package com.mrc.monami.multibhashi.home;

import com.mrc.monami.multibhashi.model.ApiData;
import com.mrc.monami.multibhashi.model.LessonDatum;

public interface LessonView {

    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getLessonDataSuccess(ApiData lessonDatum);
}
