package com.mrc.monami.multibhashi.home;

import com.mrc.monami.multibhashi.model.ApiData;
import com.mrc.monami.multibhashi.model.LessonDatum;
import com.mrc.monami.multibhashi.networking.NetworkError;
import com.mrc.monami.multibhashi.networking.NetworkService;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class LessonPresenter {
    private final NetworkService service;
    private final LessonView lessonView;
    private CompositeSubscription compositeSubscription;


    public LessonPresenter(NetworkService service, LessonView lessonView) {
        this.service = service;
        this.lessonView = lessonView;
        this.compositeSubscription = new CompositeSubscription();
    }

    public void getLessonData(){
        lessonView.showWait();
        Subscription subscription = service.getLessonData(new NetworkService.GetLessonDataCallback() {
            @Override
            public void onSuccess(ApiData lessonDatum) {
                lessonView.removeWait();
                lessonView.getLessonDataSuccess(lessonDatum);
            }

            @Override
            public void onError(NetworkError networkError) {
                lessonView.removeWait();
                lessonView.onFailure(networkError.getAppErrorMessage());

            }
        });
        compositeSubscription.add(subscription);

    }
    public void onStop(){
        compositeSubscription.unsubscribe();
    }
}
